import React, { useState } from "react";
import Box from "./Box";

export default function Board() {
  let Message = "";
  let offline = false;

  const [Past, setPast] = useState([]);

  const [Moves, setMoves] = useState(Array(9).fill(null));

  const [Turn, setTurn] = useState(true);

  function MovesCount() {
    let TotalMoves = 0;

    for (let index = 0; index < Moves.length; index++) {
      if (Moves[index] != null) {
        TotalMoves++;
      }
    }

    return TotalMoves;
  }

  function handleClick(index) {
    if (Moves[index] !== null) {
      return;
    }

    let count = MovesCount();

    if (count < Past.length) {
      Past.splice(count);

      setPast(Past);
    }

    if (!offline) {
      const prev = [...Past];

      const copy = [...Moves];

      prev.push(copy);

      setPast(prev);

      copy[index] = Turn ? "X" : "O";

      setMoves(copy);

      setTurn(!Turn);
    }
  }
  function travelTo(index) {
    if (index === -1) {
      setMoves(Array(9).fill(null));
      setTurn(true);
    } else {
      const pastMoves = Past[index];

      setMoves(pastMoves);

      if ((index) % 2 == 0) {
        setTurn(false);
      } else {
        setTurn(true);
      }
    }
  }

  const Data = Past.map((obj, index) => {
    if (index === 0) {
      return (
        <>
          <li>
            <button onClick={() => travelTo(-1)}>Travel to {0} Move</button>
          </li>
          <li>
            <button onClick={() => travelTo(index)}>
              Travel to {index + 1} Move
            </button>
          </li>
        </>
      );
    } else {
      return (
        <li>
          <button onClick={() => travelTo(index)}>
            Travel to {index + 1} Move
          </button>
        </li>
      );
    }
  });

  function checkWinner() {
    const WinSenarios = [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],

      // Vertical check
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      // Daigonal check
      [0, 4, 8],
      [2, 4, 6],
    ];

    for (let row of WinSenarios) {
      const [a, b, c] = row;

      if (Moves[a] != null && Moves[a] === Moves[b] && Moves[b] === Moves[c]) {
        console.log("won!!!!!!!!!!!!");
        offline = true;
        return Moves[a];
      }
    }
    return false;
  }
  const isWinner = checkWinner();

  function isDraw() {
    if (!Moves.includes(null)) {
      return true;
    }
    return false;
  }

  function resetBoard() {
    const newBoard = Array(9).fill(null);
    setMoves(newBoard);
    setPast([]);
    setTurn(true);
  }
  if (isWinner) {
    Message = `Congratulation's ${isWinner}`;
  } else if (isDraw()) {
    Message = `Match Draw`;
  } else {
    Message = `It's ${Turn ? "X" : "O"}'s turn `;
  }
  return (
    <div className="board">
      <h1>{Message}</h1>
      {isDraw() || isWinner ? (
        <>
          <button className="Restartbutton" onClick={resetBoard}>
            <h3>Restart</h3>
          </button>
        </>
      ) : (
        <></>
      )}
      <div className="buttonRow">
        <Box played={() => handleClick(0)} value={Moves[0]} />
        <Box played={() => handleClick(1)} value={Moves[1]} />
        <Box played={() => handleClick(2)} value={Moves[2]} />
      </div>
      <div className="buttonRow">
        <Box played={() => handleClick(3)} value={Moves[3]} />
        <Box played={() => handleClick(4)} value={Moves[4]} />
        <Box played={() => handleClick(5)} value={Moves[5]} />
      </div>
      <div className="buttonRow">
        <Box played={() => handleClick(6)} value={Moves[6]} />
        <Box played={() => handleClick(7)} value={Moves[7]} />
        <Box played={() => handleClick(8)} value={Moves[8]} />
      </div>
      <ol className="pastList">{Data}</ol>
    </div>
  );
}
